import Vue from 'vue'
import Router from 'vue-router'

import store from '../store'

import Home from '../views/Home'
import Login from '../views/Login'
import Signup from '../views/Signup'
import Profile from '../views/Profile'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Home,
      beforeEnter: (to, from, next) => {
        store.dispatch('fetchMatches', 'matches').then(res => next() )
        let items = localStorage.getItem('token')
        if (items === 'undefined' || items === null || items.length === 0){
          next({ path: '/login' });
        }else {
          Vue.http.interceptors.push((request, next) => {
            request.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
            request.headers.set('Accept', 'application/json')
            next()
          })
          next();
        }
      }
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/signup',
      component: Signup
    },
    {
      path: '/profile',
      component: Profile
    }
  ]
})
