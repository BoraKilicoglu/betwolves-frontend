import Vue from 'vue'
import Vuex from 'vuex'

import match from './match'
import user from './user'
import coupon from './coupon'
import system from './system'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    match: match,
    coupon: coupon,
    user: user,
    system: system
  },
})

export default store
