import Vue from 'vue'
import Vuex from 'vuex'
import service from '../services/service'
import moment from 'moment'
import _ from 'lodash'

const match = {
  state: { 
    raw: [],
    filtered: [],
    leaders: [],
    headToHead: [],
    favorites: [],
  },
  mutations: {
    FETCH_MATCHES (state, data) {
      Vue.set(state, 'raw', data);
      Vue.set(state, 'filtered', _.groupBy(state.raw, 'date'));
    },
    LEADERS (state, data) {
      state.leaders.push(data)
    },
    HEAD_TO_HEAD (state, data) {
      state.headToHead.push(data)
    },
    FAVORITES (state, data) {
      state.favorites.push(data)
    },
    FILTERS (state, data) {
      switch(data) {
        case("leaders"):
          Vue.set(state, 'filtered', _.groupBy(state.leaders, 'date'));
          break
        case("headToHead"):
          Vue.set(state, 'filtered', _.groupBy(state.headToHead, 'date'));
          break
        case("favorites"):
          Vue.set(state, 'filtered', _.groupBy(state.favorites, 'date'));
          break
        default:
          Vue.set(state, 'filtered', _.groupBy(state.raw, 'date'));
      }
    },
    SET_SELECTED (state, data) {
      let foundIndex = state.raw.findIndex(item => item.code == data.code);
      state.raw[foundIndex]["name"] = data.name;
      console.log(state.raw[foundIndex]);
    }
  },
  actions: {
    fetchMatches ({ commit }, data) {
      return service.fetchMatches(data).then(response => {
        response.map((item) => {
          item["date"] = moment.unix(item["date"]).format("DD/MM/YYYY");
          item["home"] = JSON.parse(item["odds"])["F.1"];
          item["draw"] = JSON.parse(item["odds"])["F.X"];
          item["away"] = JSON.parse(item["odds"])["F.2"];
          item["under"] = JSON.parse(item["odds"])["UNDER"];
          item["over"] = JSON.parse(item["odds"])["OVER"];
          if(item.ranking == 1)
            commit('LEADERS', item)
          if(item.ranking == 2)
            commit('HEAD_TO_HEAD', item)
          if(item.ranking == 3)
            commit('FAVORITES', item)
        });
        commit('FETCH_MATCHES', response)
      })
    },
    filters ({ commit }, data) {
      commit('FILTERS', data)
    },
    setSelected({ commit }, data) {
      commit('SET_SELECTED', data)
    }
  },
  getters: {
    raw: state => state.raw,
    filtered: state => state.filtered,
  }
}

export default match