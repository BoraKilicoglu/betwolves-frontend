import Vue from 'vue'
import Vuex from 'vuex'
import service from '../services/service'
import user from './user'

const coupon = {
  state: {
    coupon: [],
    removed: 0,
    ratios: [],
    totalRatio: 0,
    gain: 0,
    money: 3.00,
    userCoupons : {}
  },
  mutations: {
    ADD_MATCH (state, data) {
      state.totalRatio = ''
      state.coupon.push(data)
      state.ratios.push(parseFloat(data["selected"]));
      state.totalRatio = state.ratios.reduce(function(a,b){return a*b;})
    },
    REMOVE_MATCH(state, data) {
      state.totalRatio = ''
      let item = state.coupon.findIndex(x => x.code === data.code)
      state.removed = data.code
      state.coupon.splice(item, 1);
      state.ratios.splice(item, 1);
      if(state.ratios.length > 1){
        state.totalRatio = state.ratios.reduce(function(a,b){return a*b;})
      }else if(state.coupon.length == 1) {
        state.totalRatio = parseFloat(state.coupon[0].selected)
      }
    },
    DELETE_ALL(state) {
      state.coupon.map((item) => {
        Vue.set(item, 'selected', '')
        Vue.set(item, 'game', '')
      })
      state.coupon = []
      state.removed = 0
      state.ratios = []
      state.totalRatio = 0
      state.gain = 0
      state.money = 3.00
    },
    RESET_REMOVED(state) {
      state.removed = ''
    },
    UPDATE_MONEY: function (state, data) {
      state.money = data
    },
    UPDATE_GAIN: function (state, data) {
      state.gain = data
    },
    SAVED_COUPON(state) {
      // console.log("saved")
    },
    USER_COUPONS(state, data) {
      state.userCoupons = data
    }  
  },
  actions: {
    addMatch ({ commit }, data) {
      commit('ADD_MATCH', data)
    },
    removeMatch ({ commit }, data) {
      commit('REMOVE_MATCH', data)
    },
    deleteAll ({ commit }) {
      commit('DELETE_ALL')
    },
    resetRemoved ({ commit }) {
      commit('RESET_REMOVED')
    },
    saveCoupon ({state, commit }, data) {
      let payload = []
      state.coupon.map((item) => {
        let picked = (({ code, mbc, homeTeam, awayTeam, selected }) => ({ code, mbc, homeTeam, awayTeam, selected }))(item);
        payload.push(picked)
      })
      let container = []
      container.push(encodeURIComponent(JSON.stringify(payload)))
      container.push(JSON.stringify(state.money))
      container.push(JSON.stringify(state.totalRatio))
      container.push(JSON.stringify(state.gain))
      container.push(JSON.stringify(user.state.user.id))
      return service.saveCoupon(container).then(response => {
        commit('SAVED_COUPON')
      })
    },
    userCoupons({ commit }){
      return service.userCoupons(user.state.user.id).then(response => {
        commit('USER_COUPONS', response)
      })
    }
  },
  getters: {
    coupon (state) {return state.coupon},
    removed (state) {return state.removed},
    totalRatio (state) {return state.totalRatio},
    money (state) {return state.money},
    userCoupons (state) {return state.userCoupons}
  }
}

export default coupon