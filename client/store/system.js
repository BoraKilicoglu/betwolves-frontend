import Vue from 'vue'
import Vuex from 'vuex'
import coupon from './coupon'
import _ from 'lodash';
import 'lodash.combinations';

const system = {
  state: { 
    systemSelected: [],
    systemButtons: [],
    systemRatio: 0,
    systemGain: 0
  },
  mutations: {
    SYSTEM_BUTTONS (state) {
      if(coupon.state.coupon.find(x => x.mbc === 4) && coupon.state.coupon.length > 4) {
        state.systemButtons = Array.from({length:coupon.state.coupon.length - 3},(v,k)=>k+4)
      }else if(coupon.state.coupon.find(x => x.mbc === 3) && coupon.state.coupon.length > 3) {
        state.systemButtons = Array.from({length:coupon.state.coupon.length - 2},(v,k)=>k+3)
      }else {
        state.systemButtons = []
      }
      state.systemSelected.length > 0 && this.commit('SYSTEM_COMPARE')
    },
    SYSTEM_COMPARE (state) {
      if(!state.systemButtons.includes(state.systemSelected[0])){
        state.systemSelected.shift();
        this.commit('SYSTEM_RATIO')
      } 
    },
    SYSTEM_SET (state, data) {
      if(state.systemSelected.length){
        let index = state.systemSelected.findIndex(k => k == data);
        if (index !== -1) {
          state.systemSelected.splice(index, 1);
        }else {
          state.systemSelected.push(data)  
        }
      }else {
        state.systemSelected.push(data)
      }
      this.commit('SYSTEM_RATIO')
    },
    SYSTEM_RATIO (state) {
      state.systemRatio = 0
      state.systemSelected.forEach((value, i) => {
        let raw = _.combinations(coupon.state.ratios, value);             
        let combination = raw.map(nested => nested.reduce(function(a,b){return a*b;}));
        let converted = combination.map(Number)
        let transformed = converted.reduce(function(a,b){return a+b;})
        state.systemRatio += parseFloat(transformed)
      });
    }
  },
  actions: {
    systemSet ({ commit, dispatch }, data) {
      commit('SYSTEM_SET', data)
    },
    systemTrigger ({ commit, dispatch }, data) {
      commit('SYSTEM_RATIO', data)
    },
    systemButtons({ commit, dispatch}, data) {
      commit('SYSTEM_BUTTONS', data)
    }
  },
  getters: {
    systemSelected (state) {return state.systemSelected},
    systemButtons (state) {return state.systemButtons},
    systemRatio (state) {return state.systemRatio},
  }
}

export default system