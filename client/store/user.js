import Vue from 'vue'
import Vuex from 'vuex'
import service from '../services/service'

const user = {
  state: {
    user: {},
    authenticated: false,
  },
  mutations: {
    LOGOUT_USER (state, data) {
      state.authenticated = false
    },
    LOGIN_USER (state, data) {
    },
    SIGNUP_USER (state, data) {
    },
    AUTH_USER (state, data) {
      state.user = data
      state.authenticated = true
    }
  },
  actions: {
    loginUser ({ commit, dispatch }, data) {
      return service.loginUser(data).then(response => {
        commit('LOGIN_USER', response)
        dispatch('authUser');
      })
    },
    logOut ({ commit }, data) {
      commit('LOGOUT_USER')
    },
    signUp ( {commit}, data) {
      return service.signUp(data).then(response =>{
        commit('SIGNUP_USER', response)
      })
    },
    authUser ({ commit }, data) {
      return service.authUser(data).then(response => {
        commit('AUTH_USER', response)
      })
    }
  },
  getters: {
    authenticated (state) {return state.authenticated},
    user (state) {return state.user},
  }
}

export default user