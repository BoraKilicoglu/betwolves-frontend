import Vue from 'vue'
import VueResource from 'vue-resource'
import call from '../services/call'

Vue.use(VueResource)

export default {
  setHeaders (response) {
    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    localStorage.setItem('token', response.token)
    localStorage.setItem('refreshToken', response.refreshToken)
    Vue.http.interceptors.push((request, next) => {
      request.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
      request.headers.set('Accept', 'application/json')
      next()
    })
  },
  fetchMatches (data) { 
    return call.request(data).then(response => {
      return response
    })
  },
  loginUser (data) {
    let { email, password } = data.data
    let url = `login?email=${email}&password=${password}`
    return call.request(url).then(response => {
      this.setHeaders(response)
      return response
    })
  },
  signUp (data) {
    let {email, username, password} = data.data
    let url = `register?email=${email}&username=${username}&password=${password}`
    return call.request(url).then(response => {
      return response
    })
  },
  authUser () {
    let url = `user`
    return call.request(url).then(response => {
      return response
    })
  },
  saveCoupon (data) {
    let url = `coupon/create?matches=${data[0]}&money=${data[1]}&ratio=${data[2]}&gain=${data[3]}&user_id=${data[4]}`
    console.log(url)
    return call.request(url).then(response => {
      return response
    })
  },
  userCoupons (data) {
    let url = `coupon/find?user_id=${data}`
    return call.request(url).then(response => {
      console.log(response)
      return response
    })
  }
}