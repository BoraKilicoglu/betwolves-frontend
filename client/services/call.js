import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

export default {
  request(data){
    return new Promise((resolve, reject) => { 
      Vue.http.get(`http://127.0.0.1:3333/${data}`).then((response) => {
        resolve(response.body)
      }, error => {
        reject(error.body)
      })
    })
  }
}